<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use App\Property;
use App\AnalyticType;
use App\PropertyAnalytic;

class PropertyAnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(PropertyAnalytic::with('analyticType')->get());
    }

    public function summary(Request $request) {

        $validator = Validator::make($request->all(), [
            'summary_type' => ['required','string', Rule::in(['state', 'suburb', 'country'])],
            'summary_value' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $propertyAnalytics = PropertyAnalytic::with(['properties' => function ($query) use ($request) {
            $query->where($request->summary_type, $request->summary_value);
        }])->get();

        $withoutValue = 100 * $propertyAnalytics->where('value', null)->count() / $propertyAnalytics->count();

        $data = [
            'min' => $propertyAnalytics->min('value'),
            'max' => $propertyAnalytics->max('value'),
            'median' => $propertyAnalytics->median('value'),
            'perc_without_value' => $withoutValue,
            'perc_with_value' => 100 - $withoutValue
        ];

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'value' => 'sometimes|numeric',
            'analytic_type_id' => 'required|exists:analytic_types',
            'property_id' => 'required|exists:properties'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {

            DB::beginTransaction();
            $propertyAnalytic = PropertyAnalytic::create($request->only(['value', 'analytic_type_id', 'property_id']));
            DB::commit();
            return response()->json($propertyAnalytic);

        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 417);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyAnalytic $propertyAnalytic)
    {
        return response()->json($propertyAnalytic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyAnalytic $propertyAnalytic)
    {

        $validator = Validator::make($request->all(), [
            'value' => 'sometimes|numeric',
            'analytic_type_id' => 'sometimes|exists:analytic_types',
            'property_id' => 'sometimes|exists:properties'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {

            DB::beginTransaction();
            $data = $request->only(['value', 'analytic_type_id', 'property_id']);
            $propertyAnalytic->update($data);
            DB::commit();
            return response()->json($propertyAnalytic);

        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 417);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyAnalytic $propertyAnalytic)
    {
        $propertyAnalytic->delete();
        return response()->json();
    }
}
