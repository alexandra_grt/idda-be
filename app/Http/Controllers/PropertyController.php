<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use App\Property;
use Illuminate\Support\Str;
use DB;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'suburb' => 'sometimes|string',
            'state' => 'sometimes|string',
            'country' => 'sometimes|string',
            'order_by' => ['sometimes', 'string', Rule::in(['id' ,'suburb', 'state', 'country', 'guid'])],
            'order' => ['sometimes','string', Rule::in(['asc', 'desc'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->has('order_by')) {
            $data = Property::orderBy($request->order_by, $request->order ?? 'asc');
        } else {
            $data = Property::all();
        }

        $filters = $request->only(['suburb', 'state', 'country']);
        if (sizeof($filters)) {
            foreach ($filters as $filter => $value) {
                $data->where($filter, $value);
            }
        }

        return response()->json($data->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'suburb' => 'required|string',
            'state' => 'required|string',
            'country' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {

            DB::beginTransaction();
            $data = $request->only(['suburb', 'state', 'country']);
            $data['guid'] = (string) Str::uuid();
            $property = Property::create($data);
            DB::commit();
            return response()->json($property);

        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 417);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Property $property)
    {
        $validator = Validator::make($request->all(), [
            'analytics_only' => 'sometimes|boolean'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->has('analytics_only') && $request->analytics_only) {
             return response()->json($property->propertyAnalytics->load('analyticType'));
        } else {
            return response()->json($property);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        $validator = Validator::make($request->all(), [
            'suburb' => 'sometimes|string',
            'state' => 'sometimes|string',
            'country' => 'sometimes|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {

            DB::beginTransaction();
            $data = $request->only(['suburb', 'state', 'country']);
            $property->update($data);
            DB::commit();
            return response()->json($property);

        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 417);

        }
    }

    /**
     * Update analytics
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addAnalytics(Request $request, Property $property)
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required|string',
            'analytic_type_id' => 'required|exists:analytic_types'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {

            DB::beginTransaction();
            $data = $request->only(['value', 'analytic_type_id']);
            PropertyAnalytic::create([
                'value' => $request->value,
                'analytic_type_id' => $request->analytic_type_id,
                'property_id' => $property->id
            ]);
            DB::commit();
            return response()->json($property);

        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 417);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $property->delete();
        return response()->json();
    }
}
