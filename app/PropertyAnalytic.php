<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAnalytic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'analytic_type_id',
        'property_id',
    ];

    public function properties() {
        return $this->belongsTo(Property::class);
    }

    public function analyticType() {
        return $this->belongsTo(AnalyticType::class);
    }

    public function getValueAttribute($value) {
        return (float) $value;
    }

}
