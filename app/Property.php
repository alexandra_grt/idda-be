<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid',
        'suburb',
        'state',
        'country'
    ];

    public function analyticTypes() {
        return $this->hasMany(AnalyticType::class);
    }

    public function propertyAnalytics() {
        return $this->hasMany(PropertyAnalytic::class);
    }

}
