<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalyticType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'num_decimal_places',
        'units',
        'name',
        'is_numeric'
    ];

    public function properties() {
        return $this->hasMany(Property::class);
    }

}
