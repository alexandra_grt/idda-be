

## installation

1. setup db creds in .env

2. run
composer install

3. run migrations and seed db

php artisan migrate:fresh --seed

4. run

php artisan key:generate

5. run

php artisan serve